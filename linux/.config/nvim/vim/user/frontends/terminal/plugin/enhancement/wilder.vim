scriptencoding=utf-8

call wilder#setup
\  (
\    {
\      'modes': [':', '/', '?'],
\      'next_key': '<C-j>',
\      'previous_key': '<C-k>',
\      'accept_key': '<C-CR>',
\      'reject_key': '<C-c>',
\    }
\  )

call wilder#set_option
\  (
\    'pipeline',
\    [
\      wilder#branch
\      (
\        wilder#python_file_finder_pipeline
\        (
\          {
\          'file_command':
\            {
\              _, arg -> stridx(arg, '.') != -1 ?
\                ['fd', '-tf', '-H'] :
\                ['fd', '-tf']
\            },
\          'dir_command': ['fd', '-td'],
\          'filters':
\            [
\              {
\                'name': 'fuzzy_filter',
\              },
\              {
\                'name': 'difflib_sorter'
\              },
\            ],
\          }
\        ),
\        wilder#substitute_pipeline
\        (
\          {
\            'pipeline':
\              wilder#vim_search_pipeline
\              (
\                {
\                  'skip_cmdtype_check': 1,
\                }
\              ),
\          }
\        ),
\        [
\          wilder#check({_, x -> empty(x)}),
\          wilder#history(),
\        ],
\        wilder#cmdline_pipeline
\        (
\          {
\            'fuzzy': 1,
\            'fuzzy_filter': wilder#lua_fzy_filter(),
\            'sorter': wilder#python_difflib_sorter(),
\          }
\        ),
\        wilder#vim_search_pipeline()
\      ),
\    ]
\  )

call wilder#set_option
\  (
\    'renderer',
\    wilder#popupmenu_renderer
\    (
\      wilder#popupmenu_border_theme
\      (
\        {
\          'border': 'rounded',
\          'highlighter':
\            [
\              wilder#lua_fzy_highlighter(),
\            ],
\          'min_width': '50%',
\          'max_width': '100%',
\          'min_height': '20%',
\          'max_height': '50%',
\          'reverse': 0,
\          'left':
\            [
\              ' ',
\              wilder#popupmenu_devicons(),
\              wilder#popupmenu_buffer_flags
\              (
\                {
\                  'flags': ' 1 a % + - ',
\                  'icons':
\                     {
\                      'a': '',
\                      'h': '',
\                      '#': '👁',
\                      '%': '✍',
\                      '+': '',
\                      '-': '🔒',
\                      '=': '🗉',
\                    },
\                }
\              )
\            ],
\          'right':
\            [
\              ' ',
\              wilder#popupmenu_scrollbar(),
\            ],
\        }
\      )
\    )
\  )
