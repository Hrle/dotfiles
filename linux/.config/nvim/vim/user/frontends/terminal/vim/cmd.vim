function! ToBuf(cmd, ...) abort
  let l:mods = get(a:, 1, '')
  let l:win_cmd = get(a:, 2, 'new')

  redir => message
  silent execute a:cmd
  redir END
  if empty(message)
    echoerr "No output from command: '" . a:cmd  . "'."
  else
    silent execute l:mods . ' ' . l:win_cmd
    setlocal buftype=nofile bufhidden=wipe noswapfile nobuflisted 
    silent put=message
    setlocal nomodified readonly nomodifiable
  endif
endfunction
