local M = {}

local path = require "user.lib.path"
local pack = require "user.lib.pack"
local user = require "user.lib.user"

local function config_bootstrapped(bootstrapped)
  user.setup()
  if pack.setup() then
    if bootstrapped then
      pack.sync()
    else
      pack.load()
    end
  end
end

function M.config()
  path.setup()
  if not pack.is_bootstrapped() then
    config_bootstrapped(pack.bootstrap())
  else
    config_bootstrapped()
  end
end

M.config()

return M