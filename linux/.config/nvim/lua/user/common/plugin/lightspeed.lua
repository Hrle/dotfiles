-- NOTE: best for moving around - quite unique so hard to explain
-- NOTE: keymap is `s` or `S` and `f/F` and `t/T` are enhanced

local M = { "ggandor/lightspeed.nvim" }

M.requires = { "tpope/vim-repeat" }

return M
