-- NOTE: makes mappings easier and has out of the box which-key integration

-- TODO: without mapx because you can make a function that maps with API and
-- TODO: registers in which-key
-- TODO: make keymap lazy load friendly

local M = { "b0o/mapx.nvim" }

M.requires = {
  {
    "folke/which-key.nvim",
    disable = not require("user.lib.frontend").has_gui,
    config = function()
      local which_key = require "which-key"

      which_key.setup {
        marks = true,
        registers = true,
        window = {
          border = "single",
        },
      }
    end,
  },
}

M.after = "which-key.nvim"

M.module = "mapx"

M.config = function()
  local mapx = require "mapx"

  mapx.setup {
    whichkey = require("user.lib.frontend").has_gui,
  }
end

return M