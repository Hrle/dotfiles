-- NOTE: allows you to use sudo nicely in Neovim

local M = { "lambdalisue/suda.vim" }

return M
