-- NOTE: replaces selection or line with register
-- NOTE: keymap is either "gr{motion}" or "grr"

-- TODO: find lua variant

local M = { "vim-scripts/ReplaceWithRegister" }

return M
