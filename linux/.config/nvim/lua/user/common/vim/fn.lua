local M = {}

function M.global()
  local function wrap_fn(name)
    return function(...)
      local fn = require "user.lib.fn"
      fn[name](...)
    end
  end

  P = wrap_fn "print"
  L = wrap_fn "lambda"
  S = wrap_fn "print_stack"
end

return M