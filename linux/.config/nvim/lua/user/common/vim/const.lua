local M = {}

function M.global()
  local const = require "user.lib.const"

  N = const.newline
end

return M
