local o = {}

function o.global()
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"

  -- this is in the order of the options with ':opt' command

  -- 1 important

  -- don't be compatible with vi
  vim.opt.compatible = false

  -- 2 moving around, searching, patterns

  -- case insensitive
  vim.opt.ignorecase = true

  -- case sensitive when a pattern contains upper chars
  vim.opt.smartcase = true

  -- incremental search highlight
  vim.opt.incsearch = true

  -- 3 tags

  -- 4 text display

  -- set maximum scrolling offset so the cursor is always
  -- in the center of the screen
  vim.opt.scrolloff = 999

  -- 5 syntax, spelling and highlight

  -- highlight search
  vim.opt.hlsearch = true

  -- disable Vim spellchecking
  vim.opt.spell = false

  -- treat camel-cased words as separate
  vim.opt.spelloptions = "camel"

  -- 6 multiple windows

  -- 7 multiple tab pages

  -- 8 terminal

  -- 9 using the mouse

  -- 10 printing

  -- 11 messages and info

  -- 12 selecting text

  -- use system clipboard
  vim.opt.clipboard = "unnamedplus"

  -- 13 editing text

  -- wrap above 80 chars
  vim.opt.textwidth = 80

  -- wrap at 80 chars
  vim.opt.wrapmargin = 80

  -- read ":h fo-table"
  vim.opt.formatoptions = "t,c,r,o,q,n,2,m,M,1,j,p"

  -- use ~ like an operator to change casing
  vim.opt.tildeop = true

  -- join lines with '.' without adding two spaces?? this one is so weird
  vim.opt.joinspaces = false

  -- 14 tabs and indenting

  -- number of columns occupied by a tab
  vim.opt.tabstop = 2

  -- width for auto-indents
  vim.opt.shiftwidth = 2

  -- use 2 spaces when <BS> on tabs
  vim.opt.softtabstop = 2

  -- use 'shiftwidth' for '<<' and '>>'
  vim.opt.shiftround = true

  -- converts tabs to white space
  vim.opt.expandtab = true

  -- indent a new line the same amount as the line just typed
  vim.opt.autoindent = true

  -- 15 folding

  -- TODO: config
  -- 16 diff mode

  -- 17 mapping

  -- i would set noremap here, but that would probably break plugins

  -- 18 reading and writing files

  -- last line doesn't have end of line - this just sounds confusing when the
  -- character is not present
  vim.opt.endofline = false

  -- i don't know why people add end-of-line at the end of a file
  vim.opt.fixendofline = false

  -- Byte Order Mark at the beginning of a file is a Microsoft thing i think
  vim.opt.bomb = false

  -- keep a backup file before overwriting a file
  vim.opt.backup = true

  -- backup file directory so nvim doesn't create backup files all over
  -- the place
  -- double slash is to add the full path of files as name of file with some
  -- magic
  vim.opt.backupdir = path.data_backup_dir .. "//"
  fs.ensure_dir(path.data_backup_dir)

  -- 19 the swap file

  -- use swap files
  vim.opt.swapfile = true

  -- swap file directory - double slash is to add the full path of files
  -- as name
  -- of file so nvim can find that stuff
  vim.opt.directory = path.data_swap_dir .. "//"
  fs.ensure_dir(path.data_swap_dir)

  -- num of chars to update the swap file
  vim.opt.updatecount = 100

  -- time to update the swap file and CursorHold events
  vim.opt.updatetime = 300

  -- 20 command line editing

  -- 21 executing external commands

  -- 22 running make and jumping to errors

  -- 23 language specific

  -- 24 multi-byte characters

  -- 25 various

  -- load plugin scripts on startup - needed for any plugin manager
  vim.opt.loadplugins = true

  -- use g by default in substitutions
  vim.opt.gdefault = true

  -- what to save with view files
  vim.opt.viewoptions = "folds,options,cursor,curdir"

  -- view file directory
  -- double slash is to add the full path of files as name of file so
  -- nvim can find that stuff
  vim.opt.viewdir = path.data_view_dir .. "//"
  fs.ensure_dir(path.data_view_dir)

  -- session
  vim.opt.sessionoptions =
    "blank,buffers,curdir,folds,help,options,tabpages,winsize,resize,winpos,terminal"

  -- python 2 is deprecated
  vim.opt.pyxversion = 3

  -- other - not from `:opt`

  -- NOTE: don't put this in mapping
  vim.g.mapleader = " "
end

return o