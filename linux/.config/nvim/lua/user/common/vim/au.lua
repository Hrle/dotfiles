local a = {}

function a.global()
  local au = require "user.lib.autocmd"

  au = au:group_begin "user_highlight_yank"
  au:cmd("TextYankPost", "*", function()
    vim.highlight.on_yank {
      timeout = 1000,
      on_macro = true,
      on_visual = true,
    }
  end)
  au:group_end()
end

return a