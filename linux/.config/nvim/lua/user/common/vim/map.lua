local M = {}

-- global
function M.mapx()
  local mapx = require "mapx"
  local path = require "user.lib.path"
  local packer = require "packer"

  mapx.nnoremap("<leader>fj", "/\\v", {
    label = "reqex forward (j)",
  })
  mapx.nnoremap("<leader>fk", "?\\v", {
    label = "regex backward (k)",
  })

  mapx.nnoremap("<leader>hs", "<CMD>noh<CR>", {
    label = "Search",
  })

  mapx.nnoremap("<leader>rb", ":%s/\\v", {
    label = "Buffer",
  })
  mapx.vnoremap("<leader>rb", ":s/\\v", {
    label = "selection (b)",
  })

  mapx.nnoremap("<leader>aj", "$a<CR><C-c>k", {
    label = "line below (j)",
  })
  mapx.nnoremap("<leader>ak", "0i<CR><C-c>j", {
    label = "line above (k)",
  })

  mapx.nnoremap("<leader>Rv", function()
    dofile(path.conf_lua_loc)
    packer.compile()
  end, {
    label = "Vim",
  })
  mapx.nnoremap("<leader>RP", function()
    dofile(path.conf_lua_loc)
    packer.sync()
  end, {
    label = "Plugins",
  })
  -- TODO: through lua
  mapx.nnoremap("<leader>RR", function()
    vim.cmd [[
      UpdateRemotePlugins
    ]]
  end, {
    label = "Remote plugins",
  })
end

return M