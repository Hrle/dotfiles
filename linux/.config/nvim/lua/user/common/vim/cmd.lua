local M = {}

function M.global()
  local cmd = require "user.lib.cmd"

  cmd("UnloadModule", function(opt)
    local mod = require "user.lib.module"
    mod.unload_module(opt.args[1])
  end, {
    nargs = 1,
  })
end

return M
