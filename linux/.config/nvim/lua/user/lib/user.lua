local M = {}

local module = require "user.lib.module"
local paths = require "user.lib.path"
local fn = require "user.lib.fn"

local function setup_unsafe()
  module.source_pattern(paths.conf_vim_common_vim_pat)
  module.run_matching_functions(paths.conf_lua_common_vim_pat, "global$")

  module.source_pattern(paths.conf_vim_frontend_vim_pat)
  module.run_matching_functions(paths.conf_lua_frontend_vim_pat, "global$")
end

function M.setup()
  return fn.run_safely(setup_unsafe, "Failed running user config")
end

return M
