local image = {}

local function clamp(c)
  if c > 255 then
    return 255
  end
  if c < 0 then
    return 0
  end
  return c
end

local function add_ansi_codes(f, d)
  local r = {}

  local delta = math.floor(255 / #f)
  local du = delta
  local dd = -1 * delta

  local dr
  local cr
  local dg
  local cg
  local db
  local cb
  if d == "up" then
    dr, cr, dg, cg, db, cb = unpack { dd, 255, du, 0, 0, 255 }
  else
    dr, cr, dg, cg, db, cb = unpack { du, 0, dd, 255, 0, 255 }
  end

  for _, v in ipairs(f) do
    cr = clamp(cr)
    cg = clamp(cg)
    cb = clamp(cb)

    table.insert(
      r,
      "\027[38;2;" .. cr .. ";" .. cg .. ";" .. cb .. "m" .. v .. "\027[0m"
    )

    cr = cr + dr
    cg = cg + dg
    cb = cb + db
  end

  return r
end

local neovim_header = {
  [[ ███▄    █  ▓█████ ▒█████   ██▒   █▓  ██▓ ███▄ ▄███▓]],
  [[ ██ ▀█   █  ▓█   ▀▒██▒  ██▒▓██░   █▒▒▓██▒▓██▒▀█▀ ██▒]],
  [[▓██  ▀█ ██▒ ▒███  ▒██░  ██▒ ▓██  █▒░▒▒██▒▓██    ▓██░]],
  [[▓██▒  ▐▌██▒ ▒▓█  ▄▒██   ██░  ▒██ █░░░░██░▒██    ▒██ ]],
  [[▒██░   ▓██░▒░▒████░ ████▓▒░   ▒▀█░  ░░██░▒██▒   ░██▒]],
  [[░ ▒░   ▒ ▒ ░░░ ▒░ ░ ▒░▒░▒░    ░ ▐░   ░▓  ░ ▒░   ░  ░]],
  [[░ ░░   ░ ▒░░ ░ ░    ░ ▒ ▒░    ░ ░░  ░ ▒ ░░  ░      ░]],
  [[  ░   ░ ░     ░  ░ ░ ░ ▒        ░  ░ ▒ ░░      ░    ]],
  [[        ░ ░   ░      ░ ░        ░    ░         ░    ]],
}

local neovim_footer = {
  [[        ░ ░   ░      ░ ░        ░    ░         ░    ]],
  [[  ░   ░ ░     ░  ░ ░ ░ ▒        ░  ░ ▒ ░░      ░    ]],
  [[░ ░░   ░ ▒░░ ░ ░    ░ ▒ ▒░    ░ ░░  ░ ▒ ░░  ░      ░]],
  [[░ ▒░   ▒ ▒ ░░░ ▒░ ░ ▒░▒░▒░    ░ ▐░   ░▓  ░ ▒░   ░  ░]],
  [[▒██░   ▓██░▒░▒████░ ████▓▒░   ▒▀█░  ░░██░▒██▒   ░██▒]],
  [[▓██▒  ▐▌██▒ ▒▓█  ▄▒██   ██░  ▒██ █░░░░██░▒██    ▒██ ]],
  [[▓██  ▀█ ██▒ ▒███  ▒██░  ██▒ ▓██  █▒░▒▒██▒▓██    ▓██░]],
  [[ ██ ▀█   █  ▓█   ▀▒██▒  ██▒▓██░   █▒▒▓██▒▓██▒▀█▀ ██▒]],
  [[ ███▄    █  ▓█████ ▒█████   ██▒   █▓  ██▓ ███▄ ▄███▓]],
}

image.header = add_ansi_codes(neovim_header, "down")
image.footer = add_ansi_codes(neovim_footer, "up")

return image