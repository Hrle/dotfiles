local M = {}

local path = require "user.lib.path"
local module = require "user.lib.module"
local fs = require "user.lib.fs"
local fn = require "user.lib.fn"
local frontend = require "user.lib.frontend"

local function init_unsafe()
  local packer = require "packer"

  packer.init {
    package_root = path.data_pack_dir,
    compile_path = path.conf_lua_packer_compiled_loc,
    snapshot_path = path.cache_packer_snapshot_dir,
    max_jobs = 4,
    git = { clone_timeout = 300 },
    profile = { enable = true, threshold = 1 },
    display = frontend.name == frontend.terminal and {
      open_fn = function()
        return require("packer.util").float { border = "single" }
      end,
    } or frontend.name == frontend.vscode and {
      non_interactive = true,
    },
  }

  packer.reset()
end

local function declare_plugins_unsafe(plugin_pattern)
  local packer = require "packer"

  module.for_each_module(plugin_pattern, function(spec, spec_path)
    if type(spec.config) ~= "table" then
      if not spec.config then
        spec.config = {}
      else
        spec.config = { spec.config }
      end
    end

    table.insert(
      spec.config,
      string.format(
        [[ require("user.lib.module").run_matching_functions("%s", "%s") ]],
        path.conf_lua_common_vim_pat,
        module.get_module_tail(spec_path) .. "$"
      )
    )

    table.insert(
      spec.config,
      string.format(
        [[ require("user.lib.module").run_matching_functions("%s", "%s") ]],
        path.conf_lua_frontend_vim_pat,
        module.get_module_tail(spec_path) .. "$"
      )
    )

    local vim_mirror_module_loc = module.get_mirror_vim_module_path(spec_path)
    if fs.exists(vim_mirror_module_loc) then
      table.insert(
        spec.config,
        string.format(
          [[ require("user.lib.module").source("%s") ]],
          vim_mirror_module_loc
        )
      )
    end

    packer.use(spec)
  end)
end

local function declare_rocks_unsafe(rocks_pattern)
  local packer = require "packer"

  module.for_each_module(rocks_pattern, packer.use_rocks)
end

function M.is_bootstrapped()
  return fs.exists_glob(path.data_packer_install_dir)
end

function M.bootstrap()
  local did_bootstrap = vim.fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    path.data_packer_install_dir,
  }

  if not did_bootstrap then
    print("Failed bootstrapping: " .. did_bootstrap)
  end

  return did_bootstrap
end

function M.setup()
  -- NOTE: treesitter has a hard time with this
  (function(cmd)
    vim.cmd(cmd)
  end) "packadd packer.nvim"

  local initialized = fn.run_safely(init_unsafe, "Failed initializing packer")
  if not initialized then
    return false
  end

  local declared_common_plugins = fn.run_safely(
    declare_plugins_unsafe,
    "Failed declaring common plugins",
    path.conf_lua_common_plugin_pat
  )
  if not declared_common_plugins then
    return false
  end

  local declared_common_rocks = fn.run_safely(
    declare_rocks_unsafe,
    "Failed declaring common rocks",
    path.conf_lua_common_rocks_pat
  )
  if not declared_common_rocks then
    return false
  end

  local declared_frontend_plugins = fn.run_safely(
    declare_plugins_unsafe,
    "Failed declaring frontend plugins",
    path.conf_lua_frontend_plugin_pat
  )
  if not declared_frontend_plugins then
    return false
  end

  local declared_frontend_rocks = fn.run_safely(
    declare_rocks_unsafe,
    "Failed declaring frontend rocks",
    path.conf_lua_frontend_rocks_pat
  )
  if not declared_frontend_rocks then
    return false
  end

  return true
end

function M.sync()
  local packer = require "packer"

  packer.sync()
end

function M.load()
  local loaded_configs = fn.run_safely(
    dofile,
    "Failed running packer config",
    path.conf_lua_packer_compiled_loc
  )
  if not loaded_configs then
    return false
  end

  return true
end

return M
