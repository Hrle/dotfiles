local M = {}

function M.print(...)
  print(vim.inspect(...))
end

function M.run_safely(unsafe, message, ...)
  local did_run, result = pcall(unsafe, ...)
  if not did_run then
    print(message .. ": " .. result)
  end

  return did_run
end

function M.lambda(lambda, state)
  return setmetatable(state or {}, {
    __call = function(...)
      return lambda(...)
    end,
  })
end

function M.print_stack(name)
  local const = require "user.lib.const"

  print(name, ":")
  local index = 1
  local current = debug.getinfo(index)

  print(const.newline)
  repeat
    if current.short_src == "[C]" then
      if not current.name then
        print(index, "<compiled>", "<lambda>")
      else
        print(index, "<compiled>", current.name)
      end
    elseif not current.name then
      print(index, "<lambda>", current.currentline, current.short_src)
    else
      print(index, current.name, current.currentline, current.short_src)
    end
    index = index + 1
    current = debug.getinfo(index)
  until current == nil
  print(const.newline)
end

return M
