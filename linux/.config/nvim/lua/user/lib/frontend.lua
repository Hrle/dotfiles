local M = {}

-- NOTE: with undersocre because NvimTree doesn't like vscode
M.vscode = "vs_code"
M.terminal = "terminal"

if vim.g.vscode then
  M.name = M.vscode
  M.has_gui = false
else
  M.name = M.terminal
  M.has_gui = true
end

function M.enable_on(...)
  for _, frontend in ipairs { ... } do
    if frontend == M.name then
      return true
    end
  end

  return false
end

return M