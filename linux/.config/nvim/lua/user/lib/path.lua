local M = {}

local frontend = require "user.lib.frontend"

-- global

M.conf_root_dir = vim.fn.stdpath "config"

M.conf_lua_loc = M.conf_root_dir .. "/init.lua"
M.conf_vim_loc = M.conf_root_dir .. "/init.vim"

M.conf_lua_root_dir = M.conf_root_dir .. "/lua/user"
M.conf_vim_root_dir = M.conf_root_dir .. "/vim/user"

M.conf_lua_lib_dir = M.conf_lua_root_dir .. "/lib"
M.conf_lua_lib_pat = M.conf_lua_root_dir .. "/lib/**/*.lua"
M.conf_lua_lib_vim_pat = M.conf_lua_root_dir .. "/lib/*.lua"

M.data_root_dir = vim.fn.stdpath "data"
M.data_site_dir = M.data_root_dir .. "/site"

M.data_swap_dir = M.data_root_dir .. "/swap"
M.data_undo_dir = M.data_root_dir .. "/undo"
M.data_backup_dir = M.data_root_dir .. "/backup"
M.data_view_dir = M.data_root_dir .. "/view"
M.data_session_dir = M.data_root_dir .. "/session"

M.cache_root_dir = vim.fn.stdpath "cache"

M.plantuml_jar_loc = "/usr/share/java/plantuml/plantuml.jar"

-- common

M.conf_lua_common_root_dir = M.conf_root_dir .. "/lua/user/common"

M.conf_lua_common_plugin_dir = M.conf_lua_common_root_dir .. "/plugin"
M.conf_lua_common_plugin_pat = M.conf_lua_common_root_dir .. "/plugin/**/*.lua"
M.conf_lua_common_plugin_vim_pat = M.conf_lua_common_root_dir .. "/plugin/*.lua"

M.conf_lua_common_rocks_dir = M.conf_lua_common_root_dir .. "/rocks"
M.conf_lua_common_rocks_pat = M.conf_lua_common_root_dir .. "/rocks/**/*.lua"
M.conf_lua_common_rocks_vim_pat = M.conf_lua_common_root_dir .. "/rocks/*.lua"

M.conf_lua_common_vim_dir = M.conf_lua_common_root_dir .. "/vim"
M.conf_lua_common_vim_pat = M.conf_lua_common_root_dir .. "/vim/**/*.lua"
M.conf_lua_common_vim_vim_pat = M.conf_lua_common_root_dir .. "/vim/*.lua"

M.conf_lua_common_lang_dir = M.conf_lua_common_root_dir .. "/lang"
M.conf_lua_common_lang_pat = M.conf_lua_common_root_dir .. "/lang/**/*.lua"
M.conf_lua_common_lang_vim_pat = M.conf_lua_common_root_dir .. "/lang/*.lua"

M.conf_vim_common_root_dir = M.conf_root_dir .. "/vim/user/common"

M.conf_vim_common_plugin_dir = M.conf_vim_root_dir .. "/plugin"
M.conf_vim_common_plugin_pat = M.conf_vim_root_dir .. "/plugin/**/*.vim"
M.conf_vim_common_plugin_vim_pat = M.conf_vim_root_dir .. "/plugin/*.vim"

M.conf_vim_common_vim_dir = M.conf_vim_common_root_dir .. "/vim"
M.conf_vim_common_vim_pat = M.conf_vim_common_root_dir .. "/vim/**/*.vim"
M.conf_vim_common_vim_vim_pat = M.conf_vim_common_root_dir .. "/vim/*.vim"

M.conf_vim_common_lang_dir = M.conf_vim_common_root_dir .. "/lang"
M.conf_vim_common_lang_pat = M.conf_vim_common_root_dir .. "/lang/**/*.vim"
M.conf_vim_common_lang_vim_pat = M.conf_vim_common_root_dir .. "/lang/*.vim"

-- frontend

M.conf_lua_frontend_root_dir = M.conf_root_dir
  .. ("/lua/user/frontends/%s"):format(frontend.name)

M.conf_lua_frontend_plugin_dir = M.conf_lua_frontend_root_dir .. "/plugin"
M.conf_lua_frontend_plugin_pat = M.conf_lua_frontend_root_dir
  .. "/plugin/**/*.lua"
M.conf_lua_frontend_plugin_vim_pat = M.conf_lua_frontend_root_dir
  .. "/plugin/*.lua"

M.conf_lua_frontend_rocks_dir = M.conf_lua_frontend_root_dir .. "/rocks"
M.conf_lua_frontend_rocks_pat = M.conf_lua_frontend_root_dir
  .. "/rocks/**/*.lua"
M.conf_lua_frontend_rocks_vim_pat = M.conf_lua_frontend_root_dir
  .. "/rocks/*.lua"

M.conf_lua_frontend_vim_dir = M.conf_lua_frontend_root_dir .. "/vim"
M.conf_lua_frontend_vim_pat = M.conf_lua_frontend_root_dir .. "/vim/**/*.lua"
M.conf_lua_frontend_vim_vim_pat = M.conf_lua_frontend_root_dir .. "/vim/*.lua"

M.conf_lua_frontend_lang_dir = M.conf_lua_frontend_root_dir .. "/lang"
M.conf_lua_frontend_lang_pat = M.conf_lua_frontend_root_dir .. "/lang/*.lua"
M.conf_lua_frontend_lang_vim_pat = M.conf_lua_frontend_root_dir .. "/lang/*.lua"

M.conf_vim_frontend_root_dir = M.conf_root_dir
  .. ("/vim/user/frontends/%s"):format(frontend.name)

M.conf_vim_frontend_plugin_dir = M.conf_vim_root_dir .. "/plugin"
M.conf_vim_frontend_plugin_pat = M.conf_vim_root_dir .. "/plugin/**/*.vim"
M.conf_vim_frontend_plugin_vim_pat = M.conf_vim_root_dir .. "/plugin/*.vim"

M.conf_vim_frontend_vim_dir = M.conf_vim_frontend_root_dir .. "/vim"
M.conf_vim_frontend_vim_pat = M.conf_vim_frontend_root_dir .. "/vim/**/*.vim"
M.conf_vim_frontend_vim_vim_pat = M.conf_vim_frontend_root_dir .. "/vim/*.vim"

M.conf_vim_frontend_lang_dir = M.conf_vim_frontend_root_dir .. "/lang"
M.conf_vim_frontend_lang_pat = M.conf_vim_frontend_root_dir .. "/lang/*.vim"
M.conf_vim_frontend_lang_vim_pat = M.conf_vim_frontend_root_dir .. "/lang/*.vim"

-- resulting data and cache

M.data_frontend_dir = M.data_site_dir .. ("/%s"):format(frontend.name)
M.data_pack_dir = M.data_frontend_dir .. "/pack"
M.data_packer_dir = M.data_pack_dir .. "/packer"
M.data_packer_install_dir = M.data_packer_dir .. "/start/packer.nvim"

M.additional_packpath = M.data_frontend_dir
M.additional_runtimepath = M.data_packer_dir
  .. "/*/start/*"
  .. ","
  .. M.data_packer_dir
  .. "/*/opt/*"
  .. ","
  .. M.conf_lua_frontend_root_dir
  .. "/runtime"
  .. ","
  .. M.conf_vim_frontend_root_dir
  .. "/runtime"
function M.setup()
  vim.o.packpath = vim.o.packpath .. "," .. M.additional_packpath
  vim.o.runtimepath = vim.o.runtimepath .. "," .. M.additional_runtimepath
end

M.cache_frontend_dir = M.cache_root_dir .. ("/%s"):format(frontend.name)
M.cache_packer_dir = M.cache_frontend_dir .. "/packer"
M.cache_packer_snapshot_dir = M.cache_packer_dir .. "/snapshot"
M.conf_lua_packer_compiled_loc = M.cache_packer_dir .. "/compiled.lua"

return M