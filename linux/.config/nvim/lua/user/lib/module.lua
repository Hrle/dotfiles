local M = {}

function M.get_module_path(name)
  local root = require("user.lib.path").conf_root_dir .. "/lua/"
  local path = root .. string.gsub(name, ".", "/") .. ".lua"
  return path
end

function M.get_module_name(path)
  local root = require("user.lib.path").conf_root_dir .. "/lua/"

  local relative = string.gsub(path, root, "", 1)
  local extensionless = vim.fn.fnamemodify(relative, ":r")
  local name = string.gsub(extensionless, "/", ".")

  return name
end

function M.get_module_tail(path)
  return vim.fn.fnamemodify(path, ":t:r")
end

function M.get_mirror_vim_module_path(path)
  return string.gsub(path, "lua", "vim")
end

function M.unload_module(name)
  package.loaded[name] = nil
end

function M.with_module(path, todo)
  local did, result = pcall(dofile, path)
  if not did then
    print(result)
  else
    todo(result, path, M.get_module_name(path))
  end
end

function M.for_each_module(path, todo)
  local glob = vim.fn.glob(path)
  for match in string.gmatch(glob, "%S+") do
    M.with_module(match, todo)
  end
end

function M.run_matching_functions(path, pattern, ...)
  local capture = { ... }
  M.for_each_module(path, function(module)
    if type(module) == "table" then
      for key, value in pairs(module) do
        if string.match(key, pattern) and type(value) == "function" then
          value(unpack(capture))
        end
      end
    end
  end)
end

function M.source(path)
  -- NOTE: vim.fn.source doesn't work for some reason
  local did_source, result = pcall(vim.cmd, "source " .. path)
  if not did_source then
    print(result)
  end
end

function M.source_pattern(path)
  for match in string.gmatch(vim.fn.glob(path), "%S+") do
    M.source(match)
  end
end

return M
