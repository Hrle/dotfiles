local M = {}

function M.global()
  local user_ft = vim.api.nvim_create_augroup("user_ft", { clear = true })
  local function create_ft(pattern, ft)
    vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
      group = user_ft,
      pattern = pattern,
      callback = function()
        vim.bo.filetype = ft
      end,
    })
  end

  create_ft("*.cshtml", "html")
  create_ft("*.rasi", "rasi")
end

return M