local M = {}

function M.global()
  local cmd = require "user.lib.cmd"

  cmd(
    "ToBuf",
    "call ToBuf(<q-args>, <q-mods>)",
    { nargs = "+", complete = "command" }
  )

  cmd(
    "ToVBuf",
    "call ToBuf(<q-args>, <q-mods>, 'vnew')",
    { nargs = "+", complete = "command" }
  )
end

return M
