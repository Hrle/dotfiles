local a = {}

function a.lsp_buf() end

function a.formatter()
  local autocmd = vim.api.nvim_create_autocmd
  local augroup = vim.api.nvim_create_augroup

  local function format(opt)
    if not string.match(opt.file, [[.*%.vim$]]) then
      vim.cmd [[
        silent! undojoin
        FormatWriteLock
      ]]
    end
  end

  local formatter = augroup("user_formatter", { clear = true })
  autocmd("BufWritePost", {
    group = formatter,
    pattern = "*",
    callback = format,
  })

  -- NOTE: keep this here for debugging
  -- local debug = augroup("user_formatter_debug", { clear = true })
  -- autocmd("User", {
  --   group = debug,
  --   pattern = "FormatterPost",
  --   callback = function()
  --     print "Here!"
  --   end,
  -- })
end

function a.lightbulb()
  local function update(_)
    local lightbulb = require "nvim-lightbulb"
    lightbulb.update_lightbulb()
  end

  local au = require "user.lib.autocmd"

  au = au:group_begin "user_lightbulb"
  au:cmd({ "CursorHold", "CursorHoldI" }, "*", update)
  au:group_end()
end

function a.lint()
  local function lint()
    local _lint = require "lint"
    _lint.try_lint()
  end

  local au = require "user.lib.autocmd"

  au = au:group_begin "user_lint"
  au:cmd({ "BufWritePost" }, "*", lint)
  au:group_end()
end

function a.baleia()
  local baleia = require("baleia").setup { name = "ANSICode" }
  local au = require "user.lib.autocmd"

  au = au:group_begin "user_baleia"
  au:cmd("FileType", "dashboard", function()
    vim.cmd [[
      setlocal modifiable
    ]]
    baleia.once(vim.fn.bufnr "%")
    vim.cmd [[
      setlocal nomodified
      setlocal nomodifiable
    ]]
  end)
  au:group_end()
end

function a.plantuml()
  local autocmd = vim.api.nvim_create_autocmd
  local augroup = vim.api.nvim_create_augroup

  local plantuml_open = false
  local function change_plantuml_include_path()
    vim.g["plantuml_previewer#include_path"] = vim.fn.getcwd()
  end
  change_plantuml_include_path()

  local plantuml = augroup("user_plantuml", { clear = true })
  autocmd("FileType", {
    group = plantuml,
    pattern = { "plantuml" },
    callback = function()
      if plantuml_open then
        vim.cmd [[PlantumlOpen]]
        plantuml_open = true
      else
        vim.cmd [[PlantumlStart]]
      end
    end,
  })
  autocmd("DirChanged", {
    group = plantuml,
    pattern = { "global" },
    callback = change_plantuml_include_path,
  })
end

return a