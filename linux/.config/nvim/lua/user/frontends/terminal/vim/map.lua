local M = {}

-- global
function M.mapx()
  local mapx = require "mapx"

  mapx.name("<leader>", "user")

  mapx.name("<leader>D", "Do")
  mapx.name("<leader>U", "Undo")
  mapx.name("<leader>Q", "Quit")

  mapx.name("<leader>R", "Refresh")
  mapx.name("<leader>H", "Help")

  -- NOTE: local movement
  mapx.name("<leader><leader>", "jump (<leader>)")
  -- NOTE: global movement
  mapx.name("<leader>g", "Go")
  -- NOTE: moving resources
  mapx.name("<leader>m", "Move")

  -- NOTE: general/float
  mapx.name("<leader>s", "Show")
  -- NOTE: quickfix/loclist
  mapx.name("<leader>l", "List")
  mapx.name("<leader>h", "Hide")

  mapx.name("<leader>f", "Find")
  mapx.name("<leader>r", "Replace")

  mapx.name("<leader>o", "Open")
  mapx.name("<leader>c", "Close")
  mapx.name("<leader>t", "Toggle")

  -- NOTE: files/directories - stuff on disk
  mapx.name("<leader>w", "Write")
  mapx.name("<leader>e", "Erase")

  -- NOTE: buffers/characters/virtual - stuff in memory
  mapx.name("<leader>a", "Add")
  mapx.name("<leader>d", "Delete")

  mapx.nnoremap("<leader><leader>j", function()
    vim.diagnostic.goto_next()
  end, {
    label = "to next diagnostic (j)",
  })
  mapx.nnoremap("<leader><leader>k", function()
    vim.diagnostic.goto_prev()
  end, {
    label = "to previous diagnostic (k)",
  })

  mapx.nnoremap("<leader>ld", function()
    vim.diagnostic.setqflist()
  end, {
    label = "Diagnostics",
  })

  -- TODO: through lua
  mapx.nnoremap("<leader>RT", function()
    vim.cmd [[
      TSUpdate
    ]]
  end, {
    label = "Treesitter",
  })
end

-- telescope
function M.telescope()
  local mapx = require "mapx"
  local telescope = require "telescope"
  local builtin = require "telescope.builtin"
  local cheatsheet = require "cheatsheet"

  mapx.nnoremap("<leader><leader><leader>", function()
    builtin.builtin()
  end, {
    label = "menu (<leader>)",
  })

  mapx.nnoremap("<leader>Hv", function()
    builtin.help_tags()
  end, {
    label = "with Vim",
  })
  mapx.nnoremap("<leader>Hp", function()
    builtin.man_pages()
  end, {
    label = "with Programs",
  })
  mapx.nnoremap("<leader>Hk", function()
    cheatsheet.show_cheatsheet()
  end, {
    label = "with Keymaps",
  })

  mapx.nnoremap("<leader>fn", function()
    telescope.extensions.notify.notify {}
  end, {
    label = "Notification",
  })

  mapx.nnoremap("<leader>fp", function()
    telescope.extensions.project.project {}
  end, {
    label = "Project",
  })
  mapx.nnoremap("<leader>fs", function()
    telescope.extensions["session-lens"].search_session()
  end, {
    label = "Session",
  })
  mapx.nnoremap("<leader>fz", function()
    telescope.extensions.zoxide.list {}
  end, {
    label = "Zoxide entry",
  })
  mapx.nnoremap("<leader>fr", function()
    telescope.extensions.frecency.frecency()
  end, {
    label = "fRecency entry",
  })
  mapx.nnoremap("<leader>fw", function()
    builtin.live_grep()
  end, {
    label = "Workspace line",
  })

  mapx.nnoremap("<leader>ff", function()
    builtin.find_files()
  end, {
    label = "File",
  })
  mapx.nnoremap("<leader>fm", function()
    telescope.extensions.media_files.media_files()
  end, {
    label = "Media",
  })
  mapx.nnoremap("<leader>fs", function()
    builtin.lsp_workspace_symbols()
  end, {
    label = "Symbol",
  })
  mapx.nnoremap("<leader>fy", function()
    builtin.lsp_dynamic_workspace_symbols()
  end, {
    label = "dYnamic symbol",
  })
  mapx.nnoremap("<leader>fd", function()
    builtin.diagnostics()
  end, {
    label = "Diagnostic",
  })
  mapx.nnoremap("<leader>fc", function()
    telescope.extensions.dap.configurations()
  end, {
    label = "debug Configuration",
  })

  mapx.nnoremap("<leader>fb", function()
    builtin.buffers()
  end, {
    label = "Buffer",
  })

  mapx.nnoremap("<leader>fl", function()
    builtin.current_buffer_fuzzy_find()
  end, {
    label = "Line",
  })

  mapx.nnoremap("<leader>fe", function()
    builtin.symbols { sources = { "emoji" } }
  end, {
    label = "Emoji",
  })
end

-- lsp config
function M.lsp_buf(_, b)
  local mapx = require "mapx"

  vim.api.nvim_buf_set_option(b, "omnifunc", "v:lua.vim.lsp.omnifunc")

  mapx.nname("<leader>l", "language", { buffer = b })

  mapx.nnoremap("<leader>wf", function()
    vim.lsp.buf.add_workspace_folder()
  end, {
    label = "workspace Folder",
    buffer = b,
  })
  mapx.nnoremap("<leader>ef", function()
    vim.lsp.buf.remove_workspace_folder()
  end, {
    label = "workspace Folder",
    buffer = b,
  })

  mapx.nnoremap("<leader>rn", function()
    vim.lsp.buf.rename()
  end, {
    label = "Name",
    buffer = b,
  })

  -- TODO: Neoformat integration?
  -- mapx.nnoremap("<leader>df", function()
  --   vim.lsp.buf.formatting()
  -- end, {
  --   label = "format",
  --   buffer = b,
  -- })
  -- TODO: CodeActionMenu integration?
  -- mapx.nnoremap("<leader>da", function()
  --   vim.lsp.buf.code_action()
  -- end, {
  --   label = "code action",
  --   buffer = b,
  -- })

  mapx.nnoremap("<leader>gd", function()
    vim.lsp.buf.definition()
  end, {
    label = "to Definition",
    buffer = b,
  })
  mapx.nnoremap("<leader>gD", function()
    vim.lsp.buf.declaration()
  end, {
    label = "to Declaration",
    buffer = b,
  })
  mapx.nnoremap("<leader>gI", function()
    vim.lsp.buf.implementation()
  end, {
    label = "to Implementation",
    buffer = b,
  })

  mapx.nnoremap("<leader>sh", function()
    vim.lsp.buf.hover()
  end, {
    label = "Hover",
    buffer = b,
  })
  mapx.nnoremap("<leader>sf", function()
    vim.lsp.buf.signature_help()
  end, {
    label = "Function signature",
    buffer = b,
  })
  mapx.nnoremap("<leader>st", function()
    vim.lsp.buf.type_definition()
  end, {
    label = "Type",
    buffer = b,
  })

  mapx.nnoremap("<leader>li", function()
    vim.lsp.buf.incoming_calls()
  end, {
    label = "Incoming calls",
    buffer = b,
  })
  mapx.nnoremap("<leader>lo", function()
    vim.lsp.buf.outgoing_calls()
  end, {
    label = "Outgoing calls",
    buffer = b,
  })
  mapx.nnoremap("<leader>lr", function()
    vim.lsp.buf.references()
  end, {
    label = "References",
    buffer = b,
  })

  mapx.nnoremap("<leader>ah", function()
    vim.lsp.buf.document_highlight()
  end, {
    label = "Highlight",
    buffer = b,
  })
end

-- enhancements
function M.tree()
  local mapx = require "mapx"
  local tree = require "nvim-tree"
  local lib = require "nvim-tree.lib"

  mapx.nnoremap("<leader>tt", function()
    tree.toggle()
  end, {
    label = "Tree",
  })

  mapx.nnoremap("<leader>ct", function()
    tree.close()
  end, {
    label = "Tree",
  })

  mapx.nnoremap("<leader>ot", function()
    tree.open()
  end, {
    label = "Tree",
  })

  mapx.nnoremap("<leader>Rt", function()
    lib.refresh_tree()
  end, {
    label = "Tree",
  })
end

function M.bufferline()
  local mapx = require "mapx"
  local bufferline = require "bufferline"

  mapx.nnoremap("<leader><leader>b", function()
    bufferline.pick_buffer()
  end, {
    label = "to Buffer",
  })

  mapx.nnoremap("<leader>cb", function()
    bufferline.close_buffer_with_pick()
  end, {
    label = "Buffer",
  })

  mapx.nnoremap("<leader><leader>l", function()
    bufferline.cycle(1)
  end, {
    label = "to right buffer (l)",
  })

  mapx.nnoremap("<leader><leader>h", function()
    bufferline.cycle(-1)
  end, {
    label = "to left buffer (h)",
  })

  mapx.nnoremap("<leader>ml", function()
    bufferline.move(1)
  end, {
    label = "buffer to right (l)",
  })

  mapx.nnoremap("<leader>mh", function()
    bufferline.move(-1)
  end, {
    label = "buffer to left (h)",
  })

  mapx.nnoremap("<leader>tB", function()
    bufferline.toggle_bufferline()
  end, {
    label = "Bufferline",
  })
end

function M.marks()
  local mapx = require "mapx"
  local marks = require "marks"

  mapx.nnoremap("<leader>lm", function()
    marks.mark_state:global_to_list "quickfixlist"
  end, {
    label = "Marks",
  })

  mapx.nnoremap("<leader>lM", function()
    marks.mark_state:all_to_list "quickfixlist"
  end, {
    label = "all Marks",
  })
end

-- lang
-- TODO: move this somewhere safe
-- function k.neoformat()
--   local mapx = require "mapx"
--
--   mapx.nnoremap("<leader>Df", function()
--     -- TODO: filter vim files
--     vim.cmd [[
--     silent Neoformat
--     ]]
--   end, {
--     label = "Format",
--   })
-- end

function M.formatter()
  local mapx = require "mapx"

  mapx.nnoremap("<leader>Df", function()
    vim.cmd [[ FormatWrite ]]
  end, {
    label = "Format",
  })
end

function M.code_action_menu()
  local mapx = require "mapx"
  local code_action_menu = require "code_action_menu"

  mapx.nnoremap("<leader>oc", function()
    code_action_menu.open_code_action_menu()
  end, {
    label = "Code action menu",
  })

  mapx.nnoremap("<leader>cc", function()
    code_action_menu.close_code_action_menu()
  end, {
    label = "Code action menu",
  })
end

function M.lint()
  local mapx = require "mapx"
  local lint = require "lint"

  mapx.nnoremap("<leader>Dl", function()
    lint.try_lint()
  end, {
    label = "Lint",
  })
end

function M.dap()
  local mapx = require "mapx"
  local dap = require "dap"
  local ui = require "dapui"

  mapx.nnoremap("<leader>Dd", function()
    dap.continue()
  end, {
    label = "Debug",
  })
  mapx.nnoremap("<leader>Qd", function()
    dap.terminate()
  end, {
    label = "Debug",
  })

  mapx.nnoremap("<leader>gj", function()
    dap.step_into()
  end, {
    label = "in (j)",
  })
  mapx.nnoremap("<leader>gJ", function()
    dap.down()
  end, {
    label = "down (J)",
  })
  mapx.nnoremap("<leader>gk", function()
    dap.step_out()
  end, {
    label = "out (k)",
  })
  mapx.nnoremap("<leader>gK", function()
    dap.up()
  end, {
    label = "up (K)",
  })
  mapx.nnoremap("<leader>gm", function()
    dap.step_over()
  end, {
    label = "over (m)",
  })
  mapx.nnoremap("<leader>gM", function()
    dap.continue()
  end, {
    label = "to next breakpoint (M)",
  })
  mapx.nnoremap("<leader>gi", function()
    dap.step_back()
  end, {
    label = "back (i)",
  })
  mapx.nnoremap("<leader>gI", function()
    dap.reverse_continue()
  end, {
    label = "to last breakpoint (I)",
  })
  mapx.nnoremap("<leader>gc", function()
    dap.run_to_cursor()
  end, {
    label = "to Cursor",
  })

  mapx.nnoremap("<leader>ab", function()
    dap.set_breakpoint()
  end, {
    label = "Breakpoint",
  })
  mapx.nnoremap("<leader>aB", function()
    dap.set_breakpoint(vim.fn.input "Breakpoint condition: ")
  end, {
    label = "Breakpoint with condition",
  })
  mapx.nnoremap("<leader>tb", function()
    dap.toggle_breakpoint()
  end, {
    label = "Breakpoint",
  })
  mapx.nnoremap("<leader>dB", function()
    dap.clear_breakpoints()
  end, {
    label = "Breakpoints",
  })

  mapx.nnoremap("<leader>lb", function()
    dap.list_breakpoints()
  end, {
    label = "Breakpoints",
  })

  mapx.nnoremap("<leader>or", function()
    dap.repl.open()
  end, {
    label = "Repl",
  })
  mapx.nnoremap("<leader>cr", function()
    dap.repl.close()
  end, {
    label = "Repl",
  })
  mapx.nnoremap("<leader>tr", function()
    dap.repl.toggle()
  end, {
    label = "Repl",
  })

  mapx.nnoremap("<leader>ss", function()
    ui.float_element("scopes", { enter = true })
  end, {
    label = "Scopes",
  })
  mapx.vnoremap("<leader>se", function()
    ui.eval()
  end, {
    label = "Eval",
  })
end

function M.sniprun() end

-- candy
function M.gitsigns()
  local mapx = require "mapx"
  local gitsigns = require "gitsigns"

  mapx.nnoremap("<leader>lg", function()
    gitsigns.setqflist "all"
  end, {
    label = "Git diff",
  })
end

function M.gitsigns_buf(b)
  local mapx = require "mapx"
  local gitsigns = require "gitsigns"

  mapx.nnoremap("<leader>tg", function()
    gitsigns.toggle_current_line_blame()
  end, {
    label = "Git blame",
    buffer = b,
  })

  mapx.nnoremap("<leader>Dg", function()
    gitsigns.diffthis()
  end, {
    label = "Git diff",
    buffer = b,
  })
end

function M.todo_comments()
  local mapx = require "mapx"
  local search = require "todo-comments.search"

  mapx.nnoremap("<leader>lt", function()
    search.setqflist()
  end, {
    label = "Todo",
  })
end

function M.zen_mode()
  local mapx = require "mapx"
  local zen_mode = require "zen-mode"

  mapx.nnoremap("<leader>oz", function()
    zen_mode.open()
  end, {
    label = "zen mode",
  })
  mapx.nnoremap("<leader>cz", function()
    zen_mode.close()
  end, {
    label = "zen mode",
  })
  mapx.nnoremap("<leader>tz", function()
    zen_mode.toggle()
  end, {
    label = "zen mode",
  })
end

-- external
function M.neogit()
  local mapx = require "mapx"
  local neogit = require "neogit"

  mapx.nnoremap("<leader>oG", function()
    neogit.open()
  end, {
    label = "Git",
  })
end

function M.spectre()
  local mapx = require "mapx"
  local spectre = require "spectre"

  mapx.nnoremap("<leader>oR", function()
    spectre.open()
  end, {
    label = "replacer",
  })
  mapx.vnoremap("<leader>oR", function()
    spectre.open_visual()
  end, {
    label = "replacer",
  })
end

function M.glow()
  local mapx = require "mapx"
  local glow = require "glow"

  mapx.nnoremap("<leader>Dm", function()
    glow.glow ""
  end, {
    label = "render Markdown",
  })
end

-- session
function M.close_buffers()
  local mapx = require "mapx"
  local close_buffers = require "close_buffers"

  mapx.nnoremap("<leader>co", function()
    close_buffers.delete { type = "other" }
  end, {
    label = "Other buffers",
  })
  mapx.nnoremap("<leader>ch", function()
    close_buffers.delete { type = "hidden" }
  end, {
    label = "Hidden buffers",
  })
  mapx.nnoremap("<leader>cn", function()
    close_buffers.delete { type = "nameless" }
  end, {
    label = "Nameless buffers",
  })
end

-- TODO: map
function M.dashboard_buf(_) end

function M.transparent()
  local mapx = require "mapx"

  mapx.nnoremap("<leader>tT", function()
    require("transparent").toggle_transparent()
  end, {
    label = "Transparent",
  })
end

return M