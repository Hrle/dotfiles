local o = {}

function o.global()
  local fs = require "user.lib.fs"
  local path = require "user.lib.path"

  -- Neovide
  vim.g.neovide_input_use_logo = true
  vim.g.neovide_cursor_vfx_mode = "ripple"

  -- this is in the order of the options with ':opt' command

  -- 1 important

  -- 2 moving around, searching, patterns

  -- 3 tags

  -- 4 text display

  -- add line numbers
  vim.opt.number = false

  -- numbers relative to cursor
  vim.opt.relativenumber = false

  -- number column width
  vim.opt.numberwidth = 4

  -- preserve indentation in wrapped text
  vim.opt.breakindent = true

  -- make break indentation at least 2 chars, and shift breaks by 2 chars put
  -- 'showbreak' at the beginning of break indented lines
  vim.opt.breakindentopt = "min:2,shift:2,sbr"

  -- show this at the beginning of break indented lines
  vim.opt.showbreak = "↳"

  -- extra info
  vim.opt.list = true
  vim.opt.listchars = { trail = "·", tab = "⇥ " }
  vim.opt.fillchars = { fold = " ", vert = " ", eob = " " }

  -- don't redraw when executing macros
  vim.opt.lazyredraw = false

  -- 5 syntax, spelling and highlight

  -- syntax highlighting
  vim.cmd [[ syntax on ]]

  -- allow auto-indenting and plugins depending on file type
  vim.cmd [[ filetype plugin indent on ]]

  -- use true colors in terminal
  -- required for material theme plugin
  vim.opt.termguicolors = true

  -- highlight the cursor column
  vim.opt.cursorcolumn = false

  -- highlight cursor line
  vim.opt.cursorline = false

  -- set an 81 column border for good coding style
  -- NOTE: must be a string
  vim.opt.colorcolumn = "81"

  -- 6 multiple windows

  -- display one global statusline
  vim.opt.laststatus = 3

  -- on vsplit or vnew open a new window to the left of the current one
  vim.opt.splitright = false

  -- on split or new open a new window below the current one
  vim.opt.splitbelow = true

  -- 7 multiple tab pages

  -- 8 terminal

  -- 9 using the mouse

  -- 10 printing

  -- 11 messages and info

  -- 12 selecting text

  -- 13 editing text

  -- use undo files
  vim.opt.undofile = true

  -- undo file directory so nvim doesn't create undo files all over the place
  -- always starts in your $HOME directory
  -- double slash is to add the full path of files as name of file so
  -- nvim can find that stuff
  vim.opt.undodir = path.data_undo_dir .. "//"
  fs.ensure_dir(path.data_undo_dir)

  -- read ":h 'complete'"
  vim.opt.complete = ".,w,b,u,k,s,t"

  -- list completions in insert mode
  vim.opt.completeopt = "menu,menuone,noselect"

  -- 14 tabs and indenting

  -- 15 folding

  -- starting fold level
  vim.opt.foldlevel = 0

  -- maximum fold level
  vim.opt.foldnestmax = 3

  -- TODO: config
  -- 16 diff mode

  -- 17 mapping

  -- i would set noremap here, but that would probably break plugins

  -- timeout for popups to appear
  vim.opt.timeoutlen = 400

  -- 18 reading and writing files

  -- 19 the swap file

  -- 20 command line editing

  -- don't list completions in command mode because I'm using
  -- 'gelguy/wilder.nvim'
  vim.opt.wildmenu = false

  -- same as wilder
  -- TODO: configure this to a char
  -- vim.opt.wildchar = '<C-j>'

  -- see ':h 'wildmode''
  -- keeping it here in case I need it
  vim.opt.wildmode = "longest,list"

  -- ignore case when completing file names
  -- keeping it here in case I need it
  vim.opt.wildignorecase = true

  -- 21 executing external commands

  -- warn when using shell command when the buffer has changes
  vim.opt.warn = true

  -- 22 running make and jumping to errors

  -- 23 language specific

  -- 24 multi-byte characters

  -- use utf-8
  vim.opt.encoding = "utf-8"

  -- use utf-8 (buffer-local)
  vim.opt.fileencoding = "utf-8"

  -- emojis as full width
  vim.opt.emoji = true

  -- 25 various

  -- always display the column on the left
  vim.opt.signcolumn = "yes:1"

  -- other - not from `:opt`

  -- font
  vim.opt.guifont = "JetBrainsMono Nerd Font,DejaVu Sans,Noto Sans:h16"

  -- speed up scrolling
  vim.opt.ttyfast = true

  -- set quickfix text
  vim.opt.qftf = '{info -> v:lua.require("user.lib.quickfix").qftf(info)}'

  -- LSP logging
  vim.lsp.set_log_level "warn"
  if vim.fn.has "nvim-0.5.1" == 1 then
    require("vim.lsp.log").set_format_func(vim.inspect)
  end
end

return o