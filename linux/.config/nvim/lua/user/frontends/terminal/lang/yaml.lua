local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local yamlls_setup = {}

  lspconfig["yamlls"].setup(tbl.merge(setup, yamlls_setup))
end

return M
