local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local html_setup = {
    cmd = { "vscode-html-languageserver", "--stdio" },
    capabilities = {
      textDocument = {
        completion = {
          completionItem = {
            snippetSupport = true,
          },
        },
      },
    },
  }

  lspconfig["html"].setup(tbl.dmerge(setup, html_setup))

  local cssls_setup = {
    cmd = { "vscode-css-languageserver", "--stdio" },
  }

  lspconfig["cssls"].setup(tbl.merge(setup, cssls_setup))

  -- TODO: figure this out?
  -- local eslint_setup = {
  --   cmd = { "vscode-eslint-language-server", "--stdio" },
  --   root_dir = function(file, bufnr)
  --     local util = require "lspconfig.util"
  --
  --     local clients = vim.lsp.buf_get_clients(bufnr)
  --     for _, client in ipairs(clients) do
  --       if client.name == "tsserver" then
  --         return nil
  --       end
  --     end
  --     P(vim.tbl_map(function(client)
  --       return client.name
  --     end, clients))
  --
  --     return util.find_package_json_ancestor(file)
  --   end,
  -- }
  --
  -- lspconfig["eslint"].setup(tbl.merge(setup, eslint_setup))

  local jsonls_setup = {
    cmd = { "vscode-json-languageserver", "--stdio" },
    settings = { json = { schemas = require("schemastore").json.schemas() } },
  }

  lspconfig["jsonls"].setup(tbl.merge(setup, jsonls_setup))

  local tsserver_setup = {
    -- Ignores File is a CommonJS module; it may be converted to an ES module.
    -- Shamelessly stolen from:
    -- https://www.reddit.com/r/neovim/comments/nv3qh8/how_to_ignore_tsserver_error_file_is_a_commonjs/
    handlers = {
      ["textDocument/publishDiagnostics"] = function(err, result, ctx, config)
        if
          result ~= nil
          and result.diagnostics ~= nil
          and type(result.diagnostics) == "table"
        then
          local idx = 1
          while idx <= #result.diagnostics do
            if result.diagnostics[idx].code == 80001 then
              table.remove(result.diagnostics, idx)
            else
              idx = idx + 1
            end
          end
        end
        vim.lsp.diagnostic.on_publish_diagnostics(err, result, ctx, config)
      end,
    },
  }

  lspconfig["tsserver"].setup(tbl.merge(setup, tsserver_setup))

  local svelte_setup = {}

  lspconfig["svelte"].setup { tbl.dmerge(setup, svelte_setup) }
end

function M.format()
  vim.g.neoformat_enabled_javascript = { "prettier" }
  vim.g.neoformat_enabled_javascriptreact = { "prettier" }

  vim.g.neoformat_enabled_typescript = { "prettier" }
  vim.g.neoformat_enabled_typescriptreact = { "prettier" }
end

---query
M.ecma_injections = [[
(comment) @jsdoc
(comment) @comment
(regex_pattern) @regex

; =============================================================================
; languages

; {lang}`<{lang}>`
(call_expression
function: ((identifier) @language)
arguments: ((template_string) @content
  (#offset! @content 0 1 0 -1)
  (#inject_without_children! @content)))

; gql`<graphql>`
(call_expression
function: ((identifier) @_name
  (#eq? @_name "gql"))
arguments: ((template_string) @graphql
  (#offset! @graphql 0 1 0 -1)
  (#inject_without_children! @graphql)))

; hbs`<glimmer>`
(call_expression
function: ((identifier) @_name
  (#eq? @_name "hbs"))
arguments: ((template_string) @glimmer
  (#offset! @glimmer 0 1 0 -1)
  (#inject_without_children! @glimmer)))

; =============================================================================
; styled-components

; styled.div`<css>`
(call_expression
function: (member_expression
  object: (identifier) @_name
    (#eq? @_name "styled"))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled(Component)`<css>`
(call_expression
function: (call_expression
  function: (identifier) @_name
    (#eq? @_name "styled"))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled.div.attrs({ prop: "foo" })`<css>`
(call_expression
function: (call_expression
  function: (member_expression
    object: (member_expression
      object: (identifier) @_name
        (#eq? @_name "styled"))))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; styled(Component).attrs({ prop: "foo" })`<css>`
(call_expression
function: (call_expression
  function: (member_expression
    object: (call_expression
      function: (identifier) @_name
        (#eq? @_name "styled"))))
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))

; createGlobalStyle`<css>`
(call_expression
function: (identifier) @_name
  (#eq? @_name "createGlobalStyle")
arguments: ((template_string) @css
  (#offset! @css 0 1 0 -1)
  (#inject_without_children! @css)))
]]

function M.syntax()
  -- NOTE: no JSX parser
  vim.treesitter.query.set_query("javascript", "injections", M.ecma_injections)
  vim.treesitter.query.set_query("typescript", "injections", M.ecma_injections)
  vim.treesitter.query.set_query("tsx", "injections", M.ecma_injections)
end

return M
