local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local graphql_setup = {}

  lspconfig["graphql"].setup(tbl.merge(setup, graphql_setup))
end

return M
