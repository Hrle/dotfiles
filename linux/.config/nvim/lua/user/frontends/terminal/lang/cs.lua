local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local omnisharp_setup = {
    handlers = {
      ["textDocument/definition"] = require("omnisharp_extended").handler,
    },
    cmd = {
      "omnisharp",
      "--languageserver",
      "--hostPID",
      tostring(vim.fn.getpid()),
    },
  }

  lspconfig["omnisharp"].setup(tbl.merge(setup, omnisharp_setup))
end

function M.debug()
  local debug = require "dap"

  debug.adapters["netcoredbg"] = {
    type = "executable",
    command = "/usr/bin/netcoredbg",
    args = { "--interpreter=vscode" },
  }

  debug.configurations.cs = {
    {
      type = "netcoredbg",
      name = "attach - netcoredbg",
      request = "attach",
      processId = function()
        local pgrep_result = vim.fn.system "pgrep -f 'dotnet ./'"
        print("Running netcoredbg on:", pgrep_result)
        return tonumber(pgrep_result)
      end,
    },
  }
end

return M
