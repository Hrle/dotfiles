local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"
  local sqls = require "sqls"

  local function wrap_sqlite_connection(file)
    return "file:/opt/src/hrle/" .. file .. "?cache=shared" .. "&mode=memory"
  end

  local sqls_setup = {
    on_attach = function(client, buffer)
      setup.on_attach(client, buffer)
      sqls.on_attach(client, buffer)
    end,
    settings = {
      sqls = {
        -- connectins = {
        --   driver = "sqlite3",
        --   dataSourceName = wrap_sqlite_connection "ozds-web/UgpWeb/App_Data/Sites/Default/yessql.db",
        -- },
      },
    },
  }

  lspconfig["sqls"].setup(tbl.merge(setup, sqls_setup))
end

return M