local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local solargraph_setup = {}

  lspconfig["solargraph"].setup(tbl.merge(setup, solargraph_setup))
end

return M
