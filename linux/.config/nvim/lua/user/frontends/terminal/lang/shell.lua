local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local bashls_setup = {}

  lspconfig["bashls"].setup(tbl.merge(setup, bashls_setup))
end

return M
