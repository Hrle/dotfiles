local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local pyright_setup = {}

  lspconfig["pyright"].setup(tbl.merge(setup, pyright_setup))
end

function M.debug()
  local dap = require "dap"

  dap.adapters["python"] = {
    type = "executable",
    command = "python",
    args = { "-m", "debugpy.adapter" },
  }

  dap.configurations["python"] = {
    {
      type = "python",
      request = "launch",
      name = "launch ${file} - python",
      program = "${file}",
      pythonPath = function()
        local cwd = vim.fn.getcwd()
        if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
          return cwd .. "/venv/bin/python"
        elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
          return cwd .. "/.venv/bin/python"
        else
          return "/usr/bin/python"
        end
      end,
    },
  }
end

return M
