local M = {}

function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"
  local status = require "lsp-status"

  local clangd_setup = {
    cmd = {
      "clangd",
      "--background-index",
      "--enable-config",
      "--cross-file-rename",
      "--clang-tidy",
    },
    handlers = status.extensions.clangd.setup(),
  }

  lspconfig["clangd"].setup(tbl.merge(setup, clangd_setup))

  local cmake_setup = {}

  lspconfig["cmake"].setup(tbl.merge(setup, cmake_setup))
end

function M.debug()
  local debug = require "dap"

  debug.adapters["lldb"] = {
    type = "executable",
    command = "lldb-vscode", -- adjust as needed
    name = "lldb",
  }
end

return M
