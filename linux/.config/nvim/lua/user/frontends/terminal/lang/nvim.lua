local M = {}

-- TODO: lua debug
function M.lsp(setup)
  local tbl = require "user.lib.table"
  local lspconfig = require "lspconfig"

  local vimls_setup = {}

  lspconfig["vimls"].setup(tbl.merge(setup, vimls_setup))

  local lua_runtime_path = vim.split(package.path, ";")
  table.insert(lua_runtime_path, "lua/?.lua")
  table.insert(lua_runtime_path, "lua/?/init.lua")

  local sumneko_lua_setup = {
    cmd = { "lua-language-server" },
    settings = {
      Lua = {
        runtime = { version = "LuaJIT", path = lua_runtime_path },
        diagnostics = { globals = { "vim" } },
        workspace = {
          library = vim.api.nvim_get_runtime_file("", true),
        },
      },
    },
  }

  lspconfig["sumneko_lua"].setup(tbl.merge(setup, sumneko_lua_setup))
end

return M
