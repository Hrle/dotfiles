-- NOTE: renders git signs in buffers

-- TODO: configure more?

local p = { "lewis6991/gitsigns.nvim" }

p.requires = { "nvim-lua/plenary.nvim" }

p.config = function()
  local gitsigns = require "gitsigns"
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  gitsigns.setup {
    keymaps = nil,
    on_attach = function(buffer)
      mod.run_matching_functions(
        path.conf_lua_common_vim_pat,
        "gitsigns_buf",
        buffer
      )

      mod.run_matching_functions(
        path.conf_lua_frontend_vim_pat,
        "gitsigns_buf",
        buffer
      )
    end,
  }
end

return p
