-- NOTE: makes highlight groups transparent on demand

-- TODO: make your own colorscheme at this point...

local p = { "xiyaowong/nvim-transparent" }

function p.config()
  local transparent = require "transparent"

  transparent.setup {
    enable = true,
    groups = {
      "Normal",
      "Comment",
      "Constant",
      "Special",
      "Identifier",
      "Statement",
      "PreProc",
      "Type",
      "String",
      "Function",
      "Conditional",
      "Repeat",
      "Operator",
      "Structure",
      "LineNr",
      "NonText",
      "CursorLineNr",
      "EndOfBuffer",
      "SignColumn",
      "EndOfBuffer",
      "BufferLineTabClose",
      "BufferlineBufferSelected",
      "BufferLineFill",
      "BufferLineBackground",
      "BufferLineSeparator",
      "BufferLineIndicatorSelected",
      "NvimTreeNormal",
      "Pmenu",
      "PmenuSbar",
    },
  }
end

return p
