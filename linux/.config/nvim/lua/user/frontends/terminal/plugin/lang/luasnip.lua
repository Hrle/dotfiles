-- NOTE: adds LSP/custom snippets

local M = { "L3MON4D3/LuaSnip" }

M.config = function()
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  mod.run_matching_functions(path.conf_lua_common_lang_pat, "snippet")
  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "snippet")
end

return M
