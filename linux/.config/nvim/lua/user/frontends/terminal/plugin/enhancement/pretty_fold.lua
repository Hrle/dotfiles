-- NOTE: preview of folded region

-- TODO: configure

local M = { "anuvyklack/pretty-fold.nvim" }

M.requires = "anuvyklack/nvim-keymap-amend"

M.config = function()
  local pretty_fold = require "pretty-fold"
  local pretty_fold_preview = require "pretty-fold.preview"

  pretty_fold.setup {}
  pretty_fold_preview.setup {}
end

return M