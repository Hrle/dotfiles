-- NOTE: displays lines that indicate current scope

local p = { "lukas-reineke/indent-blankline.nvim" }

function p.config()
  local indent = require "indent_blankline"

  indent.setup {
    show_current_context = true,
    buftype_exclude = { "help", "terminal" },
    filetype_exclude = { "dashboard", "packer", "NvimTree", "Telescope" },
  }
end

return p
