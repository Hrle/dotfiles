-- NOTE: my favorite colorscheme

local M = { "marko-cerovac/material.nvim" }

M.config = function()
  local material = require "material"

  vim.g.material_style = "palenight"
  material.setup {
    italics = { comments = true, keywords = true },
    high_visibility = { darker = true },
    disable = {
      eob_lines = true,
      background = true,
    },
    custom_highlights = {
      Identifier = { fg = "#FFEFFF" },
    },
  }

  -- idk why it doesn't work like this
  -- material.set()
  vim.cmd [[ colorscheme material ]]
end

return M
