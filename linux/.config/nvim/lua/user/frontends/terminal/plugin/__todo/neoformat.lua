-- NOTE: exports a command that formats the current buffer with pre-configured formatters

-- TODO: use formatter.nvim instead

local p = { "sbdchd/neoformat" }

p.disable = true

p.config = function()
  vim.g.neoformat_basic_format_retab = 1
  vim.g.neoformat_basic_format_trim = 1
  vim.g.neoformat_try_node_exe = 1

  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "format")
end

return p
