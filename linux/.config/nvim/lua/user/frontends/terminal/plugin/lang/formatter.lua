-- NOTE: formatter

local M = { "mhartington/formatter.nvim" }

-- TODO: document transformer feature
M.config = function()
  local formatter = require "formatter"
  local filetypes = require "formatter.filetypes"
  local util = require "formatter.util"

  -- TODO: in lang dir
  formatter.setup {
    filetype = {
      ["*"] = {
        filetypes.any.remove_trailing_whitespace,
      },
      lua = {
        filetypes.lua.stylua,
      },
      javascript = {
        filetypes.javascript.prettier,
      },
      typescript = {
        filetypes.typescript.prettier,
      },
      javascriptreact = {
        filetypes.javascriptreact.prettier,
      },
      typescriptreact = {
        filetypes.typescriptreact.prettier,
      },
      svelte = {
        filetypes.svelte.prettier,
      },
      html = {
        function()
          if util.get_current_buffer_file_extension() == "cshtml" then
            return
          end

          return filetypes.html.prettier()
        end,
      },
      css = {
        filetypes.css.prettier,
      },
      cpp = {
        filetypes.cpp.clangformat,
      },
      json = {
        filetypes.json.prettier,
      },
      c = {
        filetypes.c.clangformat,
      },
      python = {
        filetypes.python.black,
      },
      cmake = {
        filetypes.cmake.cmakeformat,
      },
      yaml = {
        filetypes.yaml.prettier,
      },
      ruby = {
        filetypes.ruby.rubocop,
      },
      -- NOTE: clangformat is bad and dotnetformat is slow
      -- cs = {
      --   filetypes.cs.clangformat,
      -- },
    },
  }

  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  mod.run_matching_functions(path.conf_lua_common_lang_pat, "format")
  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "format")
end

return M
