-- NOTE: Neovim LSP configuration collection

local M = { "neovim/nvim-lspconfig" }

M.requires = {
  -- NOTE: generates status line components for LSP
  { "nvim-lua/lsp-status.nvim" },

  -- NOTE: completion integration
  { "hrsh7th/cmp-nvim-lsp" },

  -- NOTE: JSON Schema completions - configured for JSON only
  { "b0o/schemastore.nvim" },

  -- NOTE: type annotations in virtual text
  { "Hrle97/virtual-types.nvim", branch = "custom" },

  -- NOTE: improved integration with sqls
  { "nanotee/sqls.nvim" },

  -- NOTE: improved 'textDocument/definition' with omnisharp
  { "Hoffs/omnisharp-extended-lsp.nvim" },
}

M.config = function()
  local mod = require "user.lib.module"
  local path = require "user.lib.path"
  local status = require "lsp-status"
  local tbl = require "user.lib.table"
  local cmp = require "cmp_nvim_lsp"
  local virtual_types = require "virtualtypes"

  local function config(pattern)
    mod.run_matching_functions(pattern, "lsp", {
      on_attach = function(client, buffer)
        mod.run_matching_functions(
          path.conf_lua_frontend_vim_pat,
          "lsp_buf",
          client,
          buffer
        )

        status.on_attach(client)

        if client.resolved_capabilities.code_lens then
          virtual_types.on_attach(client, buffer)
        end
      end,
      capabilities = cmp.update_capabilities(
        tbl.merge(
          vim.lsp.protocol.make_client_capabilities() or {},
          status.capabilities
        )
      ),
    })
  end

  config(path.conf_lua_common_lang_pat)
  config(path.conf_lua_frontend_lang_pat)
end

return M
