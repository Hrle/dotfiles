-- NOTE: adds a lightbulb icon when a code action is available

local M = { "kosayoda/nvim-lightbulb" }

M.config = function()
  local lightbulb = require "nvim-lightbulb"

  lightbulb.update_lightbulb {
    float = { enabled = true },
    virtual_text = { enabled = true },
    status_text = { enabled = true },
  }
end

return M
