-- NOTE: adds a lint command that is configurable for a lot of languages

local M = { "mfussenegger/nvim-lint" }

function M.config()
  local lint = require "lint"
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  -- TODO: in lang dir
  lint.linters_by_ft = {
    markdown = { "vale", "codespell" },
    rst = { "vale", "codespell" },
    text = { "vale", "codespell" },
    help = { "codespell" },
    cpp = { "cppcheck", "codespell" },
    c = { "cppcheck", "codespell" },
    cs = { "codespell" },
    lua = { "luacheck", "codespell" },
    python = { "codespell" },
    vim = { "vint", "codespell" },
    bash = { "shellcheck", "codespell" },
    ruby = { "ruby", "codespell" },
  }

  mod.run_matching_functions(path.conf_lua_common_lang_pat, "lint")
  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "lint")
end

return M
