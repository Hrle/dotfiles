-- NOTE: a tabline that renders only buffers (I like using WM for tabs)

-- TODO: better keymap

local p = { "akinsho/bufferline.nvim" }

p.config = function()
  local bufferline = require "bufferline"

  local function right_custom_area()
    local result = {}
    local bufnr = vim.fn.bufnr "%"
    local error = vim.diagnostic.get(
      bufnr,
      { severity = vim.diagnostic.severity.ERROR }
    )
    local warning = vim.diagnostic.get(
      bufnr,
      { severity = vim.diagnostic.severity.WARN }
    )
    local info = vim.diagnostic.get(
      bufnr,
      { severity = vim.diagnostic.severity.INFO }
    )
    local hint = vim.diagnostic.get(
      bufnr,
      { severity = vim.diagnostic.severity.HINT }
    )

    if #error ~= 0 then
      table.insert(result, { text = "  " .. #error, guifg = "#FF5874" })
    end

    if #warning ~= 0 then
      table.insert(result, { text = "  " .. #warning, guifg = "#FFEB95" })
    end

    if #hint ~= 0 then
      table.insert(result, { text = "  " .. #hint, guifg = "#89DDFF" })
    end

    if #info ~= 0 then
      table.insert(result, { text = "  " .. #info, guifg = "#82AAFF" })
    end

    if next(result) == nil then
      table.insert(result, { text = " ✓ ", guifg = "#C3E88D" })
    end

    return result
  end

  local function diagnostics_indicator(count, level)
    local icon = level:match "error" and " "
      or level:match "warning" and ""
      or ""
    return " " .. icon .. count
  end

  bufferline.setup {
    options = {
      numbers = "none",
      diagnostics = "nvim_lsp",
      diagnostics_indicator = diagnostics_indicator,
      show_buffer_close_icons = false,
      offsets = {
        {
          filetype = "NvimTree",
          text = function()
            return string.format("%s", vim.fn.getcwd())
          end,
          highlight = "Directory",
          text_align = "center",
        },
      },
      sort_by = "extension",
      custom_areas = { right = right_custom_area },
    },
  }
end

return p
