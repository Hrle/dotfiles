-- NOTE: displays a nice dashboard when you start Neovim

-- TODO: move to startup.nvim because this is not maintained

local p = { "glepnir/dashboard-nvim" }

p.disable = true

p.requires = { "nvim-lua/telescope.nvim" }

function p.config()
  local image = require "user.lib.image"

  vim.g.dashboard_default_executive = "telescope"
  vim.g.dashboard_custom_header = image.header
  vim.g.dashboard_custom_footer = image.footer
end

return p
