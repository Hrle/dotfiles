-- NOTE: big daddy of Neovim directory tree

-- TODO: learn more mappings

local M = { "kyazdani42/nvim-tree.lua" }

M.requires = { "kyazdani42/nvim-web-devicons" }

M.config = function()
  local special_md_txt_files = {
    "README",
    "CHANGELOG",
    "LICENSE",
    "SUPPORT",
    "SECURITY",
    "CODE_OF_CONDUCT",
    "CONTRIBUTING",
    "CONTRIBUTORS",
    "AUTHORS",
    "ACKNOWLEDGMENTS",
    "CODEOWNERS",
    "ISSUE_TEMPLATE",
    "PULL_REQUEST_TEMPLATE",
  }
  local special_files = {}
  for _, special_file in pairs(special_md_txt_files) do
    special_files[special_file] = 1
    special_files[special_file .. ".txt"] = 1
    special_files[special_file .. ".md"] = 1
  end

  local tree = require "nvim-tree"
  if not vim.g.user_nvim_tree_setup then
    tree.setup {
      hijack_cursor = true,
      update_cwd = false,
      respect_buf_cwd = true,
      diagnostics = { enable = true },
      update_focused_file = { enable = true },
      renderer = {
        add_trailing = true,
        highlight_git = true,
        highlight_opened_files = "icon",
        group_empty = false,
        special_files = special_files,
        icons = {
          show = {
            git = true,
            folder = true,
            file = true,
            folder_arrow = true,
          },
        },
      },
      view = {
        width = 40,
        hide_root_folder = true,
        signcolumn = "no",
        -- NOTE: this is cool but it drives me insane sometimes
        -- adaptive_size = true,
        side = "right",
        mappings = {
          list = {
            { key = "d", action = "trash" },
            { key = "D", action = "remove" },
          },
        },
      },
      git = {
        enable = true,
        ignore = true,
        timeout = 400,
      },
      filters = {
        custom = {
          "^\\.cache",
          "^\\.git",
          "^\\.vscode",
          "^\\.idea",
          "^build",
          "^artifacts",
          "^node_modules",
        },
      },
      trash = {
        cmd = 'trash --trash-dir="$TRASH"',
      },
    }
    vim.g.user_nvim_tree_setup = true
  end
end

return M