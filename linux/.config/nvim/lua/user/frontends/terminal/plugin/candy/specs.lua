-- NOTE: adds nice animations when your cursor moves

local M = { "edluffy/specs.nvim" }

function M.config()
  local specs = require "specs"

  specs.setup {
    show_jumps = true,
  }
end

return M
