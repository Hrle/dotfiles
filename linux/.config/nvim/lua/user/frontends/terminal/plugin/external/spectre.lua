-- NOTE: opens a new window in which you can do project wide regex replacements

-- TODO: configure/learn keymaps

local M = { "windwp/nvim-spectre" }

M.requires = { "nvim-lua/plenary.nvim", "kyazdani42/nvim-web-devicons" }

function M.config()
  local spectre = require "spectre"

  spectre.setup {}
end

return M
