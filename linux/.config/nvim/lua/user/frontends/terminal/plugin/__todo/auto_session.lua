-- NOTE: remembers session per `cwd`

-- TODO: fix?

local p = { "rmagatti/auto-session" }

p.disable = true

function p.config()
  local path = require "user.lib.path"
  local fs = require "user.lib.fs"
  local auto_session = require "auto-session"

  fs.ensure_dir(path.data_session_dir)
  auto_session.setup {
    auto_session_root_dir = path.data_session_dir,
    auto_session_enabled = true,
    auto_save_enabled = true,
    auto_restore_enabled = false,
    auto_session_enable_last_session = true,
  }

  local function restore_nvim_tree()
    local nvim_tree = require "nvim-tree.lib"

    nvim_tree.change_dir(vim.fn.getcwd())
    nvim_tree.refresh_tree()
  end

  auto_session.setup {
    pre_restore_cmds = { restore_nvim_tree },
  }
end

return p
