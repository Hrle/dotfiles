-- NOTE: a universal test runnner with support for many test runners

-- TODO: configure/learn
-- TODO: checkout https://github.com/xeluxee/competitest.nvim alternative

local p = { "rcarriga/vim-ultest" }

p.disable = true

p.requires = "vim-test/vim-test"

p.run = ":UpdateRemotePlugins"

p.config = function()
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "test")
end

return p
