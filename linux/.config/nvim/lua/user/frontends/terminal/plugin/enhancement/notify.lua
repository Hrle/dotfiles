-- NOTE: enhancement for notifications

-- TODO: PR for positioning

local M = { "rcarriga/nvim-notify" }

M.config = function()
  local notify = require "notify"

  notify.setup {
    background_colour = "#000000"
  }
  vim.notify = notify
end

return M
