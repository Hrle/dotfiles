-- NOTE: Emacs Magit clone - a Git client for Neovim

-- TODO: learn more

local p = { "TimUntersberger/neogit" }

p.requires = { "nvim-lua/plenary.nvim" }

p.config = function()
  local neogit = require "neogit"

  neogit.setup {}
end

return p
