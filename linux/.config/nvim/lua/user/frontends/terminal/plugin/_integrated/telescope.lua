-- NOTE: Neovim menu

-- TODO: better keymap and command palette

local M = { "nvim-telescope/telescope.nvim" }

M.requires = {
  -- NOTE: library
  { "nvim-lua/popup.nvim" },

  -- NOTE: library
  { "nvim-lua/plenary.nvim" },

  -- NOTE: fzy sorter
  {
    "nvim-telescope/telescope-fzy-native.nvim",
    requires = { { "romgrk/fzy-lua-native", run = "make" } },
  },

  -- NOTE: fzf sorter
  { "nvim-telescope/telescope-fzf-native.nvim", run = "make" },

  -- NOTE: frecency picker
  { "nvim-telescope/telescope-frecency.nvim", requires = "tami5/sqlite.lua" },

  -- NOTE: project picker
  { "nvim-telescope/telescope-project.nvim" },

  -- NOTE: LSP symbol picker
  { "nvim-telescope/telescope-symbols.nvim" },

  -- NOTE: media file picker
  {
    "nvim-telescope/telescope-media-files.nvim",
    requires = { "nvim-lua/popup.nvim", "nvim-lua/plenary.nvim" },
  },

  -- NOTE: session picker
  -- TODO: fix?
  {
    "rmagatti/session-lens",
    disable = true,
    requires = "rmagatti/auto-session",
    config = function()
      local lens = require "session-lens"
      lens.setup {
        path_display = { "shorten" },
        previewer = true,
      }
    end,
  },

  -- NOTE: zoxide file picker
  {
    "jvgrootveld/telescope-zoxide",
  },

  -- NOTE: mapping/command picker
  {
    "sudormrfbin/cheatsheet.nvim",
    config = function()
      local cheatsheet = require "cheatsheet"
      local actions = require "cheatsheet.telescope.actions"

      cheatsheet.setup {
        telescope_mappings = {
          ["<CR>"] = actions.select_or_execute,
          ["<A-CR>"] = actions.select_or_fill_commandline,
        },
      }
    end,
  },

  -- NOTE: command picker
  {
    "LinArcX/telescope-command-palette.nvim",
  },

  -- NOTE: debug configuration picker
  {
    "nvim-telescope/telescope-dap.nvim",
  },

  -- NOTE: notification picker/viewer
  { "rcarriga/nvim-notify" },
}

M.config = function()
  local telescope = require "telescope"

  -- NOTE: try to detect binaries and not preview them - requires `file`
  local function buffer_previewer_maker(filepath, bufnr, opts)
    local previewers = require "telescope.previewers"
    local Job = require "plenary.job"

    filepath = vim.fn.expand(filepath)
    -- TODO: don't use native commands
    Job
      :new({
        command = "file",
        args = { "--mime-type", "-b", filepath },
        on_exit = function(j)
          local mime_type = vim.split(j:result()[1], "/")[1]
          if mime_type == "text" then
            previewers.buffer_previewer_maker(filepath, bufnr, opts)
          else
            vim.schedule(function()
              vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, { "BINARY" })
            end)
          end
        end,
      })
      :sync()
  end

  telescope.setup {
    defaults = {
      buffer_previewer_maker = buffer_previewer_maker,
      preview = {
        timeout = 500,
      },
    },
    mappings = {
      ["<CR>"] = require("telescope.actions").select_vertical,
    },
    extensions = {
      fzy_native = {
        override_generic_sorter = true,
        override_file_sorter = false,
      },
      fzf = {
        fuzzy = true,
        override_generic_sorter = false,
        override_file_sorter = true,
        case_mode = "smart_case",
      },
      project = { base_dirs = { "/opt/src/" .. os.getenv "USER" } },
      media_files = {
        file = {
          "png",
          "jpg",
          "jpeg",
          "gif",
          "webm",
          "webp",
          "mp4",
          "pdf",
          "epub",
        },
      },
    },
  }

  telescope.load_extension "fzf"
  telescope.load_extension "fzy_native"
  telescope.load_extension "frecency"
  telescope.load_extension "project"
  -- TODO: fix?
  -- telescope.load_extension "session-lens"
  telescope.load_extension "zoxide"
  telescope.load_extension "command_palette"
  telescope.load_extension "dap"
  telescope.load_extension "notify"
end

return M