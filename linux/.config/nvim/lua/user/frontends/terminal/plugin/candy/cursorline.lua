local p = { "yamatsum/nvim-cursorline" }

p.config = function()
  local cursorline = require "nvim-cursorline"

  cursorline.setup {
    cursorline = {
      enable = false,
      timeout = 400,
      number = false,
    },
    cursorword = {
      enable = true,
      min_length = 3,
      hl = { underline = true },
    },
  }
end

return p