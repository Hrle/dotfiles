-- NOTE: Neovim syntax

-- TODO: better keymap and textobjects

local M = { "nvim-treesitter/nvim-treesitter" }

M.requires = {
  -- NOTE: add rainbow colors to pairs
  { "p00f/nvim-ts-rainbow", after = "nvim-treesitter" },

  -- NOTE: automatically add/delete/cut HTML/XML/... tags
  { "windwp/nvim-ts-autotag", after = "nvim-treesitter" },

  -- NOTE: sets the commentstring depending on the cursor position
  { "JoosepAlviste/nvim-ts-context-commentstring", after = "nvim-treesitter" },

  -- NOTE: adds treesitter powered text objects
  { "nvim-treesitter/nvim-treesitter-textobjects", after = "nvim-treesitter" },

  -- NOTE: adds nice virtual text at the end of the current scope
  { "haringsrob/nvim_context_vt", after = "nvim-treesitter" },

  -- NOTE: makes developing queries easier
  { "nvim-treesitter/playground", after = "nvim-treesitter" },

  -- NOTE: comment selection, range, a line
  {
    "numToStr/Comment.nvim",
    after = "nvim-treesitter",

    config = function()
      local comment = require "Comment"

      comment.setup {
        pre_hook = function(ctx)
          local utils = require "Comment.utils"

          local location = nil
          if ctx.ctype == utils.ctype.block then
            location =
              require("ts_context_commentstring.utils").get_cursor_location()
          elseif
            ctx.cmotion == utils.cmotion.v or ctx.cmotion == utils.cmotion.V
          then
            location =
              require("ts_context_commentstring.utils").get_visual_start_location()
          end

          return require("ts_context_commentstring.internal").calculate_commentstring {
            key = ctx.ctype == utils.ctype.line and "__default"
              or "__multiline",
            location = location,
          }
        end,
      }
    end,
  },

  -- NOTE: automatically adds a closing pair
  {
    "windwp/nvim-autopairs",
    after = "nvim-treesitter",
    config = function()
      local autopairs = require "nvim-autopairs"

      autopairs.setup {
        disable_filetype = { "TelescopePrompt", "NvimTree" },
        check_ts = true,
      }
    end,
  },
}

M.run = ":TSUpdate"

function M.config()
  local mod = require "user.lib.module"
  local path = require "user.lib.path"
  local treesitter_config = require "nvim-treesitter.configs"

  treesitter_config.setup {
    ensure_installed = "all",
    highlight = {
      enable = true,
    },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
      },
    },
    indent = {
      enable = true,
    },
    rainbow = {
      enable = true,
      extended_mode = true,
      colors = {
        "#92aaff",
        "#99ddff",
        "#d792ea",
      },
    },
    autotag = {
      enable = true,
    },
    context_commentstring = {
      enable = true,
      enable_autocmd = false,
    },
    textobjects = {
      select = {
        enable = true,
        lookahead = true,
      },
      swap = {
        enable = true,
      },
      move = {
        enable = true,
        set_jumps = true,
      },
    },
    playground = {
      enable = true,
    },
    query_linter = {
      enable = true,
      use_virtual_text = true,
      lint_events = { "BufWrite", "CursorHold" },
    },
  }

  -- NOTE: vim.opt stuff doesn't work
  -- TODO: in lua?
  vim.cmd [[
    set foldmethod=expr
    set foldexpr=nvim_treesitter#foldexpr()
  ]]

  mod.run_matching_functions(path.conf_lua_common_lang_pat, "syntax")
  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "syntax")
end

return M
