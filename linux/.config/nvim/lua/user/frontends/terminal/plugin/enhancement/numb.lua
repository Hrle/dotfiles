-- NOTE: peeks jump location when using `:{number}`

local M = { "nacro90/numb.nvim" }

function M.config()
  local numb = require "numb"
  numb.setup {}
end

return M
