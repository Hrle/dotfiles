-- NOTE: highlights text around cursor scope

-- TODO: keybind/configure with zen mode

local p = { "folke/twilight.nvim" }

function p.config()
  local focus = require "twilight"

  focus.setup {
    context = 20,
    exclude = {
      "dashboard",
    },
  }
end

return p
