-- NOTE: provides functionality for opening a web browser from Vim

local p = { "tyru/open-browser.vim" }

return p
