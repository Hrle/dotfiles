-- NOTE: automattically adds doc comments

-- TODO: map require("neogen").generate()
-- TODO: map require("neogen").jump_next()

local p = { "danymat/neogen" }

p.disable = true

p.requires = { "nvim-treesitter/nvim-treesitter" }

function p.config()
  local neogen = require "neogen"
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  neogen.setup {
    enabled = true,
  }

  neogen.generate_command()

  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "doc")
end

return p
