-- NOTE: adds a scrollbar

local p = { "dstein64/nvim-scrollview" }

p.config = function()
  local scrollview = require "scrollview"

  scrollview.setup {
    excluded_filetypes = {
      "NvimTree",
      "TelescopePrompt",
    },
    winblend = 100,
    base = "left",
    column = 84,
  }

  -- FIX: thumb color
  -- vim.cmd [[
  --   highlight link ScrollView PmenuThumb
  -- ]]
end

return p
