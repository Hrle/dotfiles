-- NOTE: lots of enhancements for the wild menu with really nice docs

-- TODO: try cmp alternative

local M = { "gelguy/wilder.nvim" }

M.requires = {
  { "romgrk/fzy-lua-native", run = "make" },
  { "kyazdani42/nvim-web-devicons" },
}

M.rocks = { "pcre2" }

M.run = ":UpdateRemotePlugins"

return M
