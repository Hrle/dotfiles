-- NOTE: displays a nice dashboard when you start Neovim

-- TODO: fix when available

local p = { "startup-nvim/startup.nvim" }

p.requires = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }

p.disable = true

p.config = function()
  local startup = require "startup"

  startup.setup {}
end

return p