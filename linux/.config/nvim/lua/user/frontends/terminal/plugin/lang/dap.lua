-- NOTE: runs a debugger through the Debug Adapter Protocol (DAP)

-- NOTE: autocmd require'dap.ext.vscode'.load_launchjs()
-- au on cwd change and check if launchjs exists
-- au on cwd change run a lua script in the project - would be nice to add
-- project specific configurations epecially for cmake projects
-- DirChanged is the event name FYI

-- NOTE: map vnoremap <M-k> <Cmd>lua require("dapui").eval()<CR>
-- NOTE: map require("dapui").toggle()

-- NOTE: au FileType dap-repl lua require('dap.ext.autocompl').attach()
-- view h: dap.repl.open() for commands

-- NOTE: require'dap'.status()
-- add to lualine when debugging

local M = { "mfussenegger/nvim-dap" }

M.requires = {
  {
    "theHamsta/nvim-dap-virtual-text",
    config = function()
      local vt = require "nvim-dap-virtual-text"
      vt.setup {
        enabled = true,
        commented = true,
      }
    end,
  },
  {
    "rcarriga/nvim-dap-ui",
    config = function()
      local ui = require "dapui"

      ui.setup {}
    end,
  },
}

function M.config()
  local dap = require "dap"
  local ui = require "dapui"
  local tree = require "nvim-tree.view"

  -- TODO: check and figure out a good terminal strategy
  dap.defaults.fallback.terminal_win_cmd = "belowright vnew"
  dap.defaults.fallback.exception_breakpoints = "uncaught"

  -- TODO: au on cwd when launch.json exists
  -- local dap_ext_vscode = require "dap.ext.vscode"

  dap.listeners.after.event_initialized["config"] = function()
    ui.open()
    tree.close()
  end
  dap.listeners.before.event_terminated["config"] = function()
    ui.close()
    dap.repl.close()
    tree.open()
  end
  dap.listeners.before.event_exited["config"] = function()
    ui.close()
    dap.repl.close()
    tree.open()
  end -- dap_ext_vscode.load_launchjs()

  local path = require "user.lib.path"
  local mod = require "user.lib.module"

  mod.run_matching_functions(path.conf_lua_common_lang_pat, "debug")
  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "debug")
end

return M
