-- NOTE: opens the current buffer in a new neatly focused buffer

-- TODO: keybind/config with twilight

local p = { "folke/zen-mode.nvim" }

p.requires = "folke/twilight.nvim"

function p.config()
  local zen = require "zen-mode"

  zen.setup {
    window = {
      backdrop = 0.8,
      width = 140,
    },
    plugins = {
      twilight = { enabled = false },
    },
    options = {
      colorcolumn = false,
    },
  }
end

return p
