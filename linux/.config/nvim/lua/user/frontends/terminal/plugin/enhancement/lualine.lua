-- NOTE: a statusline

-- TODO: render only one statusline for all buffers?

-- FIX: colors

local M = { "hoob3rt/lualine.nvim" }

M.requires = {
  "kyazdani42/nvim-web-devicons",
  "nvim-treesitter/nvim-treesitter",
}

M.config = function()
  local lualine = require "lualine"

  local function treesitter_statusline()
    local treesitter = require "nvim-treesitter"

    return treesitter.statusline {
      indicator_size = 90,
    }
  end

  lualine.setup {
    options = {
      theme = "material-nvim",
      section_separators = { left = "", right = "" },
      component_separators = { left = "", right = "" },
      disabled_filetypes = {
        "TelescopePrompt",
        "packer",
        "dashboard",
      },
    },
    sections = {
      lualine_a = { "mode" },
      lualine_b = { treesitter_statusline },
      lualine_c = {},
      lualine_x = {},
      lualine_y = { "filename", "filetype" },
      lualine_z = { "branch", "diff" },
    },
    inactive_sections = {
      lualine_a = {},
      lualine_b = {},
      lualine_c = { "filename" },
      lualine_x = { "location" },
      lualine_y = {},
      lualine_z = {},
    },
  }

  -- NOTE: lualine does some weird thing with this option, so better to set it
  -- NOTE: properly here as well
  -- vim.opt.laststatus = 3
end

return M
