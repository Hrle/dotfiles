-- NOTE: adds nice virtual text at the end of the current scope

local M = { "haringsrob/nvim_context_vt" }

M.requires = { "nvim-treesitter" }

M.after = "nvim-treesitter"

function M.config() end

return M
