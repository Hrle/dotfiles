-- NOTE: adds a LSP statusline component

-- TODO: use more?
-- FIX: background color on status

local M = { "nvim-lua/lsp-status.nvim" }

function M.config()
  local status = require "lsp-status"

  status.register_progress()

  status.config {}
end

return M
