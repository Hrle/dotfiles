-- NOTE: enhances mark management

local M = { "chentoast/marks.nvim" }

function M.config()
  local marks = require "marks"

  marks.setup {
    default_mappings = true,
    force_write_shada = true,
  }
end

return M
