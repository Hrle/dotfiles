-- NOTE: fixes the awful display of diagnostics

-- TODO: configure nicely

local M = { "Hrle97/nvim.diagnostic_virtual_text_config" }

M.config = function()
  local diagnostic_virtual_text_config =
    require "nvim.diagnostic_virtual_text_config"
  diagnostic_virtual_text_config.setup {}
end

return M