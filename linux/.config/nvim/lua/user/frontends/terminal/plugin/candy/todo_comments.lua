-- NOTE: renders todo comments licely and can throw them in the loclist

local p = { "folke/todo-comments.nvim" }

p.config = function()
  local todo_comments = require "todo-comments"

  todo_comments.setup {}
end

return p
