-- NOTE: colorizes hexadecimal codes for colors

local M = { "norcalli/nvim-colorizer.lua" }

function M.config()
  local colors = require "colorizer"

  colors.setup {
    "*",
  }
end

return M
