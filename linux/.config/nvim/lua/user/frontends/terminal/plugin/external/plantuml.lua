-- NOTE: adds plantuml syntax highlighting, detection, indentation...

local M = { "weirongxu/plantuml-previewer.vim" }

M.requires = { "aklt/plantuml-syntax", "tyru/open-browser.vim" }

M.config = function()
  local path = require "user.lib.path"

  vim.g["plantuml_previewer#plantuml_jar_path"] = path.plantuml_jar_loc
  vim.g["plantuml_previewer#save_format"] = "png"
end

return M
