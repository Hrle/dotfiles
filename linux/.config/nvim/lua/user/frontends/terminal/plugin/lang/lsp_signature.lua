-- NOTE: adds a doc window on top of function signatures

local M = { "ray-x/lsp_signature.nvim" }

M.config = function()
  local lsp_signature = require "lsp_signature"

  lsp_signature.setup {
    bind = true,
    handler_opts = { border = "single" },
    floating_window = false,
  }
end

return M
