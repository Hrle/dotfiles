-- NOTE: standalone UI for LSP - no clutter on your statusline

-- TODO: configure servers other than sumneko lua?

local M = { "https://github.com/j-hui/fidget.nvim" }

M.config = function()
  local fidget = require "fidget"

  fidget.setup {}
end

return M