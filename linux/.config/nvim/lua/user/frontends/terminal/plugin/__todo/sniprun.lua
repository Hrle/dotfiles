-- NOTE: runs selected code on the fly

-- TODO: config/learn

local p = { "michaelb/sniprun" }

p.disable = true

p.run = "bash install.sh"

p.config = function()
  local mod = require "user.lib.module"
  local path = require "user.lib.path"

  mod.run_matching_functions(path.conf_lua_frontend_lang_pat, "sniprun")
end

return p
