-- NOTE: renders color ANSI escpae codes

local M = { "m00qek/baleia.nvim" }

function M.confg()
  local baleia = require "baleia"

  baleia.setup {
    name = "ANSICode",
  }
end

return M
