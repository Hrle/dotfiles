-- NOTE: makes the quickfix window preview files and adds other goodies to it

-- TODO: learn/modify keymaps

local p = { "kevinhwang91/nvim-bqf" }

p.requires = {
  {
    "junegunn/fzf",
    run = function()
      vim.fn["fzf#install"]()
    end,
  },
}

p.config = function() end

return p
