-- NOTE: close a lot of tabs at once

local p = { "kazhala/close-buffers.nvim" }

p.requires = { "akinsho/bufferline.nvim" }

function p.config()
  local close = require "close_buffers"

  close.setup {
    preserve_window_layout = { "this" },
    next_buffer_cmd = function(windows)
      require("bufferline").cycle(1)
      local bufnr = vim.api.nvim_get_current_buf()

      for _, window in ipairs(windows) do
        vim.api.nvim_win_set_buf(window, bufnr)
      end
    end,
  }
end

return p
